#include "utils.h"
#include "version.h"

#include <stdlib.h>

void codejs_printf(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	std::cout << CodeJS::returnJSString(args) << std::endl;
}

void codejs_rand(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	int n;
	std::random_device r;

	std::default_random_engine e1(r());
	std::uniform_int_distribution<int> uniform_dist(0, RAND_MAX);

	n = uniform_dist(e1);

	std::cout << n << std::endl;
}

void codejs_version(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	std::cout << CODEJS_VERSION << std::endl;
}

v8::Local<v8::Context> CreateShellContext(v8::Isolate* isolate)
{
	v8::Local<v8::ObjectTemplate> global = v8::ObjectTemplate::New(isolate);

	CodeJS::bindJSFunction(isolate, global, codejs_printf, "printf");
	CodeJS::bindJSFunction(isolate, global, codejs_rand, "rand");
	CodeJS::bindJSFunction(isolate, global, codejs_version, "version");

	return v8::Context::New(isolate, NULL, global);
}

int main(int argc, char *argv[])
{
	v8::V8::InitializeICUDefaultLocation(argv[0]);
	v8::V8::InitializeExternalStartupData(argv[0]);

	if (argv[1] == NULL)
		CodeJS::printError("File not is specified");

	std::unique_ptr<v8::Platform> platform = v8::platform::NewDefaultPlatform();

	std::ifstream file(argv[1], std::ifstream::in);

	std::cout << "Loading Code.js (C language + Node.js) " << CODEJS_VERSION << std::endl;

	v8::V8::InitializePlatform(platform.get());
	v8::V8::Initialize();
	v8::V8::SetFlagsFromCommandLine(&argc, argv, true);

	std::cout << "Welcome to Code.js! A Node.js with C functions!" << std::endl;
	std::cout << "JavaScript was a mistake..." << std::endl;

	v8::Isolate::CreateParams create_params;
	create_params.array_buffer_allocator = v8::ArrayBuffer::Allocator::NewDefaultAllocator();
	v8::Isolate* isolate = v8::Isolate::New(create_params);
	{
		v8::Isolate::Scope isolate_scope(isolate);
		v8::HandleScope handle_scope(isolate);
		v8::Local<v8::Context> context = CreateShellContext(isolate);

		try {
			if (context.IsEmpty())
			 	throw std::runtime_error("V8 context is empty");
		} catch (const std::exception &e) {
			std::cout << e.what() << std::endl;
			throw;
		}

		v8::Context::Scope context_scope(context);

		if (file.fail())
			CodeJS::printError("File not found");

		std::string jsfile((std::istreambuf_iterator<char>(file)),
			std::istreambuf_iterator<char>());

		if (jsfile.empty())
			CodeJS::printError("File is empty");

		v8::Local<v8::String> source = v8::String::NewFromUtf8(isolate,
						jsfile.c_str(),
						v8::NewStringType::kNormal)
						.ToLocalChecked();

		v8::Local<v8::Script> script =
			v8::Script::Compile(context, source).ToLocalChecked();

		script->Run(context).ToLocalChecked();
	}

	isolate->Dispose();
	v8::V8::Dispose();
	v8::V8::ShutdownPlatform();

	return 0;
}
