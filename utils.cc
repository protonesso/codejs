#include "utils.h"

namespace CodeJS {

void printBase(const char *msg, codejs_print_t n)
{
	std::string msg_internal = msg;

	try {
		if (msg_internal.empty())
			throw std::runtime_error("String is empty");

		switch (n) {
			case CODEJS_PRINT_ERROR:
				std::cout << "\033[31m" << "ERROR: " << "\033[37m" << msg_internal << "\033[0m" << std::endl;
				exit(EXIT_FAILURE);
				break;
			case CODEJS_PRINT_OUTPUT:
				std::cout << "\033[32m" << "OUTPUT: " << "\033[0m" << std::endl << msg_internal << std::endl;
				break;
			default:
				throw std::runtime_error("Unknown print type");
		}
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		throw;
	}
}

void printError(const char *msg)
{
	CodeJS::printBase(msg, CODEJS_PRINT_ERROR);
}

void printOutput(const char *msg)
{
	CodeJS::printBase(msg, CODEJS_PRINT_OUTPUT);
}

void bindJSFunction(v8::Isolate* isolate,
			v8::Local<v8::ObjectTemplate> global,
			v8::FunctionCallback func,
			std::string name)
{
	global->Set(
		v8::String::NewFromUtf8(isolate,
				name.c_str(),
				v8::NewStringType::kNormal)
				.ToLocalChecked(),
				 v8::FunctionTemplate::New(isolate, func));
}

std::string returnJSString(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	std::string str;

	for (int i = 0; i < args.Length(); i++) {
		v8::String::Utf8Value utf8(args.GetIsolate(), args[i]);

		str = *utf8;
	}

	return str;
}

}
