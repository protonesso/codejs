#ifndef _UTILS_H
#define _UTILS_H

#define _POSIX_C_SOURCE 200809L

#include <v8/libplatform/libplatform.h>
#include <v8/v8.h>

#include <stdexcept>
#include <memory>
#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <random>
#include <filesystem>
#include <cstdlib>

typedef enum {
	CODEJS_PRINT_ERROR,
	CODEJS_PRINT_OUTPUT
} codejs_print_t;

namespace CodeJS {
	void bindJSFunction(v8::Isolate* isolate,
			v8::Local<v8::ObjectTemplate> global,
			v8::FunctionCallback func,
			std::string name);
	void printBase(const char *msg, codejs_print_t n);
	void printError(const char *msg);
	void printOutput(const char *msg);
	std::string returnJSString(const v8::FunctionCallbackInfo<v8::Value>& args);
}

#endif
